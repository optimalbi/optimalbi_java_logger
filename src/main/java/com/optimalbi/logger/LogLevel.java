package com.optimalbi.logger;

public enum LogLevel {
    ERROR(3),
    WARN(2),
    INFO(1),
    DEBUG(0);

    private int numVal;

    LogLevel(int numVal) {

    }

    public int getNumVal() {
        return numVal;
    }
    }
