package com.optimalbi.logger;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public abstract class AbstractLogger implements Logger {
    private boolean toFormat = true;

    public void setToFormat(boolean active){
        toFormat = active;
    }

    public boolean getToFormat(){
        return toFormat;
    }

    public void debug(String message) {
        write(LogLevel.DEBUG, formatMessage(LogLevel.DEBUG,message));
    }

    public void info(String message) {
        write(LogLevel.INFO, formatMessage(LogLevel.INFO,message));
    }

    public void warn(String message) {
        write(LogLevel.WARN, formatMessage(LogLevel.WARN,message));
    }

    public void error(String message) {
        write(LogLevel.ERROR, formatMessage(LogLevel.ERROR,message));
    }

    private String formatMessage(LogLevel level, String message){
        if(!toFormat){
            return message;
        }
        ZonedDateTime now = ZonedDateTime.now();
        String ret = now.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME) + " ";
        switch (level){
            case ERROR:
                ret += "[Error] ";
                break;
            case WARN:
                ret += "[Warn] ";
                break;
            case INFO:
                ret += "[Info] ";
                break;
            case DEBUG:
                ret += "[Debug] ";
                break;
        }
        ret += message;
        return ret;
    }

    abstract protected void write(LogLevel level, String message);
}
