package com.optimalbi.logger;

public class LoggerError extends Error {
    public LoggerError(String message){
        super(message);
    }
}
