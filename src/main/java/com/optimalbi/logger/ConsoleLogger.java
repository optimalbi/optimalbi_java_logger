package com.optimalbi.logger;

import org.jetbrains.annotations.NotNull;

import java.io.PrintStream;
import java.io.PrintWriter;

public class ConsoleLogger extends AbstractLogger {
    @NotNull private PrintWriter write;
    @NotNull private PrintWriter err;

    /**
     * Constructs a normal ConsoleLogger with the normal System.out and System.err PrintStreams
     */
    public ConsoleLogger(){
        write = new PrintWriter(System.out,true);
        err = new PrintWriter(System.err,true);
    }

    /**
     * Constructs a ConsoleLogger with a custom PrintStream for both error printing and normal printing.
     * These may both be the same PrintStream if you want errors and everything else to be written to the same Streeam
     * @param write The PrintStream normal logging will be written to,
     *              error logging will not be written to this PrintStream
     * @param err The PrintStream only error logging will be written to.
     */
    public ConsoleLogger(@NotNull PrintStream write, @NotNull PrintStream err){
        this.write = new PrintWriter(write,true);
        this.err = new PrintWriter(err,true);
    }

    @Override
    protected void write(LogLevel level, String message) {
        if(level == LogLevel.ERROR){
            err.write(message + "\n");
            err.flush();
        } else {
            write.write(message + "\n");
            write.flush();
        }
    }
}
