package com.optimalbi.logger;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public final class OLog {
    public enum Loggers {
        FILE, CONSOLE
    }

    private static Logger logger = null;

    public static void debug(String message) {
        if (logger != null) {
            logger.debug(message);
        }
    }

    public static void info(String message) {
        if (logger != null) {
            logger.info(message);
        }
    }

    public static void warn(String message) {
        if (logger != null) {
            logger.warn(message);
        }
    }

    public static void error(String message) {
        if (logger != null) {
            logger.error(message);
        }
    }

    public static void SwitchLogger(Loggers logger) {
        switch (logger) {
            case FILE:
                ZonedDateTime now = ZonedDateTime.now();
                try {
                    OLog.logger = new FileLogger(now.format(DateTimeFormatter.ofPattern("yyyyMMdd")) + ".log");
                } catch (IOException e) {
                    throw new LoggerError("FileLogger could not be created");
                }
                break;
            case CONSOLE:
                OLog.logger = new ConsoleLogger();
                break;
        }
    }

    public static void SwitchLogger(Logger logger) {
        if (logger == null) {
            throw new NullPointerException("Logger must not be null");
        }
        OLog.logger = logger;
    }

    public static void AddLogger(Logger newLogger) {
        if (logger == null) {
            logger = new MultipleLevelLogger();
            ((MultipleLevelLogger) logger).AddLogger(newLogger);
        } else if (OLog.logger instanceof MultipleLevelLogger) {
            ((MultipleLevelLogger) logger).AddLogger(newLogger);
        } else {
            Logger old = logger;
            logger = new MultipleLevelLogger();
            ((MultipleLevelLogger) logger).AddLogger(old);
        }
    }

    private static Logger GetLogger() {
        return logger;
    }

    //TODO: Investigate this code
    private static String getCallerCallerClassName() {
        StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
        String callerClassName = null;
        for (int i=1; i<stElements.length; i++) {
            StackTraceElement ste = stElements[i];
            if (!ste.getClassName().equals(OLog.class.getName())&& ste.getClassName().indexOf("java.lang.Thread")!=0) {
                if (callerClassName==null) {
                    callerClassName = ste.getClassName();
                } else if (!callerClassName.equals(ste.getClassName())) {
                    return ste.getClassName();
                }
            }
        }
        return null;
    }
}
