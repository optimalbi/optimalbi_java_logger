package com.optimalbi.logger;

public interface Logger {
    boolean getToFormat();
    void setToFormat(boolean active);
    void debug(String message);
    void info(String message);
    void warn(String message);
    void error(String message);
}
