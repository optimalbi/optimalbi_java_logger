package com.optimalbi;

import com.optimalbi.logger.*;
import org.junit.*;

import java.io.File;
import java.io.IOException;


public class LoggerTests {

    @Test
    /**
     * Tests the console logger directly with a empty constructor and a basic info write
     */
    public void Console_BasicCreateWrite(){
        Logger console = new ConsoleLogger();
        console.info("This is a test of the console writer");
    }

    @Test
    public void MultipleLevelLogger_BasicCreateWrite(){
        MultipleLevelLogger multipleLevelLogger = new MultipleLevelLogger();
        multipleLevelLogger.AddLogger(new ConsoleLogger());
        try {
            multipleLevelLogger.AddLogger(new FileLogger("junittestTEMP.log"));
        } catch (IOException e) {
            Assert.fail("Could not create test log file");
        }
        multipleLevelLogger.info("Test message from junit");
        File tempFile = new File("junittestTEMP.log");
        Assert.assertTrue("Could not delete temp log file",tempFile.delete());
    }

    @Test
    public void Console_FormatOff(){
        Logger console = new ConsoleLogger();
        console.setToFormat(false);
        console.info("unformattedtext");
    }

}
