package com.optimalbi.logger;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

public class MultipleLevelLogger extends AbstractLogger {
    private Set<Logger> allLoggers;
    private Set<Logger> errorLoggers;
    private Set<Logger> debugLoggers;
    private Set<Logger> warnLoggers;
    private Set<Logger> infoLoggers;

    public MultipleLevelLogger() {
        allLoggers = new HashSet<>();
        errorLoggers = new HashSet<>();
        debugLoggers = new HashSet<>();
        warnLoggers = new HashSet<>();
        infoLoggers = new HashSet<>();
    }

    public void AddLogger(@NotNull Logger logger) {
        allLoggers.add(logger);
    }

    public void AddLogger(LogLevel level, @NotNull Logger logger) {
        switch (level) {
            case ERROR:
                errorLoggers.add(logger);
                warnLoggers.add(logger);
                infoLoggers.add(logger);
                debugLoggers.add(logger);
                break;
            case WARN:
                warnLoggers.add(logger);
                infoLoggers.add(logger);
                break;
            case INFO:
                infoLoggers.add(logger);
                debugLoggers.add(logger);
                break;
            case DEBUG:
                debugLoggers.add(logger);
                break;
        }
    }

    @Override
    protected void write(LogLevel level, String message) {
        //TODO: Needs to write to all loggers above the level not just at the level, change in storage needed
        allLoggers.forEach(logger -> ((AbstractLogger) logger).write(level, message));
        switch (level) {
            case ERROR:
                errorLoggers.forEach(logger -> ((AbstractLogger) logger).write(level, message));
                break;
            case WARN:
                warnLoggers.forEach(logger -> ((AbstractLogger) logger).write(level, message));
                break;
            case INFO:
                infoLoggers.forEach(logger -> ((AbstractLogger) logger).write(level, message));
                break;
            case DEBUG:
                debugLoggers.forEach(logger -> ((AbstractLogger) logger).write(level, message));
                break;
        }
    }
}
