package com.optimalbi.logger;

import org.jetbrains.annotations.NotNull;

import java.io.FileWriter;
import java.io.IOException;

public class FileLogger extends AbstractLogger {
    @NotNull
    private FileWriter logWriter;

    public FileLogger(@NotNull String fileName) throws IOException {
        logWriter = new FileWriter(fileName, true);
    }

    @Override
    protected void write(LogLevel level, @NotNull String message) {
        try {
            logWriter.write(message + "\n");
            logWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
